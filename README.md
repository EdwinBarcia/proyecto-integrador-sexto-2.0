PROYECTO INTEGRADOR DE SABERES SEXTO SEMESTRE
==============================================
INTEGRANTES: ANZULES AGUIRRE CLAUDIA, ARIAS ROCHA KATHERINE, BARCIA PÉREZ EDWIN, PONGUILLO CÁRDENAS CARLOS, TELLO FRANCO MELISSA
-------------------------------------------------------------------------------------------------------------------------
TEMA: EASYJOBS: APP PARA LA OBTENCIÓN DE EMPLEO EN TIEMPOS DE PANDEMIA
------------------------------------------------------------------------
IMAGENES Y EXPLICACIÓN DEL PROGRAMA
------------------------------------------------------------------------
Pantalla de Inicio de sesión
-----------------------------------------------------------------------
En esta pantalla el usuario podra loguearse ingresando su correo electrónico y su contraseña una vez que se encuentre registrado en la aplicación.

<p align = "centro" >
<img src = "https://gitlab.com/EdwinBarcia/proyecto-integrador-sexto-2.0/-/raw/master/PruebaFirebase/app/src/main/res/capturesproyecto/WhatsApp%20Image%202020-10-07%20at%2021.51.48.jpeg" alt = "Size Limit CLI" width = "200" >
</p>

Pantalla de Resgistro
----------------------------------------------------------------------
En esta pantalla el usuario se va a poder registrar poniendo cada uno de sus datos personales, esto se deberá realizar antes de poder loguearse

<p align = "centro" >
<img src = "https://gitlab.com/EdwinBarcia/proyecto-integrador-sexto-2.0/-/raw/master/PruebaFirebase/app/src/main/res/capturesproyecto/WhatsApp%20Image%202020-10-08%20at%2007.45.29.jpeg" alt = "Size Limit CLI" width = "200" >
</p>

Pantalla datos del usuario
----------------------------------------------------------------------
En esta pantalla se van a mostrar los datos de cada usuario que se haya registrado previamente
<p align = "centro" >
<img src =  "https://gitlab.com/EdwinBarcia/proyecto-integrador-sexto-2.0/-/raw/master/PruebaFirebase/app/src/main/res/capturesproyecto/WhatsApp%20Image%202020-10-08%20at%2007.53.58.jpeg" alt = "Size Limit CLI" width = "200" >
</p>

Pantalla comentarios
-----------------------------------------------------------------------
En esta pantalla se moestrarán los comentarios de los empleados y empleadores que lo que se tenga que acotar de acuerdo a su perfil laboral.
<p align = "centro" >
<img src =  "https://gitlab.com/EdwinBarcia/proyecto-integrador-sexto-2.0/-/raw/master/PruebaFirebase/app/src/main/res/capturesproyecto/WhatsApp%20Image%202020-10-08%20at%2008.03.06.jpeg" alt = "Size Limit CLI" width = "200" >
</p>
