package com.e.pruebafirebase;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pruebafirebase.Model.Comentarios;
import com.e.pruebafirebase.adapter.AdapterComentario;
import com.e.pruebafirebase.databinding.ActivityRecyclercomentariosBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListaComentario extends AppCompatActivity {
    private ActivityRecyclercomentariosBinding mainBinding;
    CollectionReference per;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityRecyclercomentariosBinding.inflate(getLayoutInflater());
        View view = mainBinding.getRoot();
        setContentView(view);
        mainBinding.reciclerPais2.setHasFixedSize(true);
        db = FirebaseFirestore.getInstance();
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            String value2 =extras.getString("id");
            consultarListaPersona(value2);

        }


    }

    public void cargar(List<Comentarios> personas) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mainBinding.reciclerPais2.setLayoutManager(layoutManager);
        RecyclerView.Adapter mAdapter = new AdapterComentario(personas);
        mainBinding.reciclerPais2.setAdapter(mAdapter);
    }



    private void consultarListaPersona(String id) {
        per = db.collection("Comentarios");
        Query query = per.whereEqualTo("idempleado", id);
        query.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            List<Comentarios> comen = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String, Object> md = document.getData();
                                String valor = (String) md.get("nombre");
                                String coment = (String) md.get("comentario");
                                comen.add(new Comentarios(valor, coment));
                            }
                            cargar(comen);

                        }
                    }
                });
    }
}