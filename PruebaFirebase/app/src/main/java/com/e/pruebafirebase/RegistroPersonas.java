package com.e.pruebafirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.e.pruebafirebase.Model.*;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class RegistroPersonas extends AppCompatActivity {
    Button b1;
    EditText nombre,apellido,correo,password,ocupacion;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registrar_personas);
        nombre = findViewById(R.id.NombrePersona);
        apellido = findViewById(R.id.ApellidoPersona);
        correo = findViewById(R.id.Email);
        db = FirebaseFirestore.getInstance();
        password = findViewById(R.id.Password);
        ocupacion = findViewById(R.id.Persona);
        b1 = findViewById(R.id.agregarPersona);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agregar();
            }
        });
    }
    public void agregar(){
        String nomb = nombre.getText().toString();
        String ape = apellido.getText().toString();
        String cor = correo.getText().toString();
        String pas = password.getText().toString();
        String ocu = ocupacion.getText().toString();
        if(nomb.equals("")||ape.equals("")||cor.equals("")||pas.equals("")){
            validation();

        }else if(ocu.equals("empleado")){
            vaciar();
            Persona p = new Persona();
            p.setId(UUID.randomUUID().toString());
            p.setNombre(nomb);
            p.setApellido(ape);
            p.setCorreo(cor);
            p.setPassword(pas);
            List<Persona> pe = new ArrayList<>();
            pe.add(p);
            for (Persona persona : pe) {
                Map<String,Object> mapa = new HashMap<>();
                mapa.put("id",persona.getId());
                mapa.put("Nombre",persona.getNombre());
                mapa.put("Apellido",persona.getApellido()        );
                mapa.put("Correo", persona.getCorreo());
                mapa.put("Password", persona.getPassword());
                db.collection("Empleado")
                        .add(mapa)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d("FB","Postulante agregado correctamente:" + documentReference.getId());
                                //Intent b1 = new Intent(MainActivity.this,LoginActivity.class);
                                //startActivity(b1);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("FB","Error Postulante no agregado",e);
                            }
                        });

            }

        }else if(ocu.equals("empleador")){
            vaciar();
            Persona p = new Persona();
            p.setId(UUID.randomUUID().toString());
            p.setNombre(nomb);
            p.setApellido(ape);
            p.setCorreo(cor);
            p.setPassword(pas);
            List<Persona> pe = new ArrayList<>();
            pe.add(p);
            for (Persona persona : pe) {
                Map<String,Object> mapa = new HashMap<>();
                mapa.put("id",persona.getId());
                mapa.put("Nombre",persona.getNombre());
                mapa.put("Apellido",persona.getApellido()        );
                mapa.put("Correo", persona.getCorreo());
                mapa.put("Password", persona.getPassword());
                db.collection("Empleador")
                        .add(mapa)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d("FB","Empleador agregado correctamente:" + documentReference.getId());
                                Intent b1 = new Intent(RegistroPersonas.this,LoginActivity.class);
                                startActivity(b1);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("FB","Error Empleador no agregado",e);
                            }
                        });

            }

        }
    }
    public void validation(){
        String nom = nombre.getText().toString();
        String ape = apellido.getText().toString();
        String cor = correo.getText().toString();
        String pas = password.getText().toString();
        String ocu = ocupacion.getText().toString();
        if(nom.equals("")){
            nombre.setError("Requiered");
        }
        if(ape.equals("")){
            apellido.setError("Requiered");
        }
        if(cor.equals("")){
            correo.setError("Requiered");
        }
        if(pas.equals("")){
            password.setError("Requiered");
        }
        if(ocu.equals("")){
            ocupacion.setError("Requiered");
        }

    }


    public void vaciar(){
        nombre.setText("");
        apellido.setText("");
        correo.setText("");
        password.setText("");
        ocupacion.setText("");
    }

}