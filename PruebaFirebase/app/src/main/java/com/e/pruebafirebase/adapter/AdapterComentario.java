package com.e.pruebafirebase.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pruebafirebase.Model.Comentarios;
import com.e.pruebafirebase.Model.Persona;
import com.e.pruebafirebase.R;
import com.e.pruebafirebase.ReciclerComentarios;

import java.util.List;

public class AdapterComentario extends RecyclerView.Adapter<AdapterComentario.MyViewHolder>  {

    public List<Comentarios> listacomen;

    public AdapterComentario(List<Comentarios> person){

        this.listacomen = person;
    }


    @NonNull
    @Override
    public AdapterComentario.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_comentarios,parent,false);
        return new AdapterComentario.MyViewHolder(v);


    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position){
        Comentarios comen=listacomen.get(position);
        holder.nombre.setText(comen.getNombre());
        holder.comentario.setText(comen.getComentario());
    }

    @Override
    public int getItemCount(){

         return listacomen.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView nombre,comentario;



        public MyViewHolder(View view){
            super(view);
            nombre= view.findViewById(R.id.NOMBRE);
            comentario=view.findViewById(R.id.COMENTARIO);



        }




    }
}
