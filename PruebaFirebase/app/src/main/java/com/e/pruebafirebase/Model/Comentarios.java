package com.e.pruebafirebase.Model;

public class Comentarios {
    private String idcomentario;
    private String idempleado;
    private String comentario;
    private String Nombre;
    private String idempleador;
    public Comentarios(){

    }

    public Comentarios(String nombre, String comentario) {
        this.comentario = comentario;
        this.Nombre = nombre;
    }

    public Comentarios(String idcomentario, String idempleado, String comentario,String Nombre, String idempleador) {
        this.idcomentario = idcomentario;
        this.idempleado = idempleado;
        this.comentario = comentario;
        this.Nombre = Nombre;
        this.idempleador = idempleador;
    }

    public String getIdcomentario() {
        return idcomentario;
    }

    public void setIdcomentario(String idcomentario) {

        this.idcomentario = idcomentario;
    }

    public String getIdempleado() {
        return idempleado;
    }

    public void setIdempleado(String idempleado) {
        this.idempleado = idempleado;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getIdempleador() {
        return idempleador;
    }

    public void setIdempleador(String idempleador) {
        this.idempleador = idempleador;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}
