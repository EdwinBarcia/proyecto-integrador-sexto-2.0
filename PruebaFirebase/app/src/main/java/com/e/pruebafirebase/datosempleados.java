package com.e.pruebafirebase;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.firestore.FirebaseFirestore;

public class datosempleados extends AppCompatActivity {
    private FirebaseFirestore db;
    Intent i1;
    Button b2;
    TextView t1, t2 , t3;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empleado);
        t1 = findViewById(R.id.setearNombreEmpleado);
        t2 = findViewById(R.id.setearApellidoEmpleado);
        t3 = findViewById(R.id.setearCorreoEmpleado);
        b2 = findViewById(R.id.agregarExperiencia);
        Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            String value1 = extras.getString("Nombre");
            String value2 = extras.getString("Apellido");
            String value3 = extras.getString("Correo");
            t1.setText(value1);
            t2.setText(value2);
            t3.setText(value3);
        }

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1 = new Intent(datosempleados.this, RegistroExperiencias.class);
                i1.putExtra("Correo", t3.getText());  
                startActivity(i1);
            }
        });



    }
}
