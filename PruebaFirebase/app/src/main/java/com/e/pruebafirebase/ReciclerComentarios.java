package com.e.pruebafirebase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;

import com.e.pruebafirebase.Model.Comentarios;
import com.e.pruebafirebase.Model.Experiencia;
import com.e.pruebafirebase.Model.Persona;
import com.e.pruebafirebase.adapter.AdapterComentario;
import com.e.pruebafirebase.adapter.AdapterPersona;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ReciclerComentarios extends AppCompatActivity {
    Context contex ;
    private FirebaseFirestore db;
    Intent i1;
    TextView t1, t2,t3,t4,t5;
    EditText e1;
    Button b1,b2;
    CollectionReference per;
    List<Experiencia> exper = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);
        t1 = findViewById(R.id.setearNombre);
        t2 = findViewById(R.id.setearCorreo);
        t3 = findViewById(R.id.seteardedicacion);
        t4 = findViewById(R.id.setearanoexperiencia);
        t5 = findViewById(R.id.seteardescripcion);
        db = FirebaseFirestore.getInstance();
        b1 = findViewById(R.id.vercomentarios);
        b2 = findViewById(R.id.Agregarcomentario);

        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            String value =extras.getString("Nombre");
            String value2 =extras.getString("Dedicacion");
            String value3 =extras.getString("Descripcion");
            consultaridempleado(value);
            t1.setText(value);
            t3.setText(value2);
            t5.setText(value3);

        }
        t2.setText(correo());
        t4.setText(anos());
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(ReciclerComentarios.this,ListaComentario.class);
                i2.putExtra("id",idempleado());
                startActivity(i2);

            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1 = new Intent(ReciclerComentarios.this, Comentario.class);
                i1.putExtra("Correo",t2.getText());
                startActivity(i1);
            }
        });
    }

    //Consultar id del empleado
    public void consultaridempleado(String correo){
        per =db.collection("Experiencias");
        Query query = per.whereEqualTo("nombre", correo);
        query.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Experiencia exp = new Experiencia();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();
                                String valor= (String) md.get("correo");
                                String valor2= (String) md.get("ano");
                                String valor3= (String) md.get("idempleado");
                                exp.setIdempleado(valor3);
                                exp.setCorreo(valor);
                                exp.setAños(valor2);
                            }
                            agregaridEmpleado(exp);

                        }
                    }
                });

    }

    public void agregaridEmpleado(Experiencia exp){
        exper.add(exp);
    }

    public String idempleado(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< exper.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return exper.get(indice).getIdempleado();

        }else {
            return null;
        }
    }

    public String correo(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< exper.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return exper.get(indice).getCorreo();

        }else {
            return null;
        }
    }

    public String anos(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< exper.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return exper.get(indice).getAños();

        }else {
            return null;
        }
    }









    /*public void agregaridEmpleador(Persona pe){
        person.add(pe);
    }*/

    /*public String agregaridempleador(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< person.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return person.get(indice).getId();

        }
        return null;
    }*/
}

