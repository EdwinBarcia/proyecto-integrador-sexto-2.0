package com.e.pruebafirebase.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.pruebafirebase.R;


import java.util.List;
import com.e.pruebafirebase.Model.*;
import com.e.pruebafirebase.ReciclerComentarios;

public class AdapterPersona extends RecyclerView.Adapter<AdapterPersona.MyViewHolder>  {

    public List<Experiencia>listaExperiencia;

    public AdapterPersona(List<Experiencia> person){

        this.listaExperiencia = person;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_personas,parent,false);
        return new MyViewHolder(v);


    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position){

        Experiencia exp=listaExperiencia.get(position);
        holder.nombre.setText(exp.getNombre());
        holder.ocupacion.setText(exp.getOcupacion());
        holder.descripcion.setText(exp.getDescripcion());
        holder.listerner();



    }

    @Override
    public int getItemCount(){

        return listaExperiencia.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        Context context;
        public TextView nombre,ocupacion,descripcion;
        public Button b1;


        public MyViewHolder(View view){
            super(view);
            context = view.getContext();
            nombre= view.findViewById(R.id.Nombre);
            ocupacion=view.findViewById(R.id.dedicacion);
            descripcion = view.findViewById(R.id.des);
            b1 = view.findViewById(R.id.ingresar);


        }

        void listerner(){
            b1.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent  i1 = new Intent(context, ReciclerComentarios.class);
            i1.putExtra("Nombre",nombre.getText());
            i1.putExtra("Dedicacion",ocupacion.getText());
            i1.putExtra("Descripcion",descripcion.getText());
            context.startActivity(i1);
        }
    }


}
