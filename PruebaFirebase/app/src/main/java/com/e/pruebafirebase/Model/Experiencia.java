package com.e.pruebafirebase.Model;

public class Experiencia {

    private String id;
    private String idempleado;
    private String nombre;
    private String correo;
    private String ocupacion;
    private String años;
    private String descripcion;

    public Experiencia(){
    }

    public Experiencia(String nombre, String ocupacion, String descripcion){
        this.nombre = nombre;
        this.ocupacion = ocupacion;
        this.descripcion = descripcion;

    }
    public Experiencia(String id, String idempleado, String nombre, String correo, String ocupacion, String años, String descripcion) {
        this.id = id;
        this.idempleado= idempleado;
        this.nombre = nombre;
        this.correo = correo;
        this.ocupacion = ocupacion;
        this.años = años;
        this.descripcion = descripcion;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdempleado() {
        return idempleado;
    }

    public void setIdempleado(String idempleado) {
        this.idempleado = idempleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getAños() {
        return años;
    }

    public void setAños(String años) {
        this.años = años;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
